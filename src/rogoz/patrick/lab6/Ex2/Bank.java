package rogoz.patrick.lab6.Ex2;

import Ex1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bank {
    private ArrayList<BankAccount>bankaccounts=new ArrayList<BankAccount>();
    void addAccount(String owner,double balance){
        BankAccount b=new BankAccount(owner,balance);
        bankaccounts.add(b);
    }

    void printAccounts(){
        Collections.sort(bankaccounts,new MyComparator());
        for(BankAccount bankaccounts:bankaccounts) {
            System.out.println(bankaccounts.getOwner()+": "+bankaccounts.balance);
        }
    }

    void printAccounts(double minBalance,double maxBalance){
        for(BankAccount bankaccounts:bankaccounts){
            if (bankaccounts.balance>minBalance && bankaccounts.balance<maxBalance)
                System.out.println(bankaccounts.getOwner()+": "+bankaccounts.balance);
        }
    }

    public ArrayList<BankAccount> getBankaccounts() {
        return bankaccounts;
    }
}



class MyComparator implements Comparator<BankAccount> {
    public int compare(BankAccount b1, BankAccount b2) {
        if (b1.getBalance() > b2.getBalance())
            return 1;
        else if (b1.getBalance() < b2.getBalance())
            return -1;
        return 0;
    }


}