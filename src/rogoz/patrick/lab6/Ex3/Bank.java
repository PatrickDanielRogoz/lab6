package rogoz.patrick.lab6.Ex3;

import Ex1.BankAccount;
import java.util.Comparator;
import java.util.*;

public class Bank {
    TreeSet<BankAccount> bankaccounts=new TreeSet<BankAccount>(new MyComparator());
    TreeSet<BankAccount> bankaccountsbyname=new TreeSet<BankAccount>(new MyNameComp());
    void addAccount(String owner,double balance){
        BankAccount b=new BankAccount(owner,balance);
        bankaccounts.add(b);
    }
    void addAccountByName(String owner,double balance){
        BankAccount b=new BankAccount(owner,balance);
        bankaccountsbyname.add(b);
    }

    void printAccounts(){
        for(BankAccount bankaccounts:bankaccounts) {
            System.out.println(bankaccounts.getOwner()+": "+bankaccounts.balance);
        }
    }
    void printAccountsByName(){
        bankaccounts=bankaccountsbyname;
        for(BankAccount bankaccounts:bankaccounts) {
            System.out.println(bankaccounts.getOwner()+": "+bankaccounts.balance);
        }
    }



    void printAccounts(double minBalance,double maxBalance){
        for(BankAccount bankaccounts:bankaccounts){
            if (bankaccounts.balance>minBalance && bankaccounts.balance<maxBalance)
                System.out.println(bankaccounts.getOwner()+": "+bankaccounts.balance);
        }
    }

    public TreeSet<BankAccount> getBankaccounts() {
        return bankaccounts;
    }
}



class MyComparator implements Comparator<BankAccount> {
    public int compare(BankAccount b1, BankAccount b2) {
        if (b1.getBalance() > b2.getBalance())
            return 1;
        else if (b1.getBalance() < b2.getBalance())
            return -1;
        return 0;
    }
}

class MyNameComp implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount b1, BankAccount b2) {
        return b1.getOwner().compareTo(b2.getOwner());
    }
}
