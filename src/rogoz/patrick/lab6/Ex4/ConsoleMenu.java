package rogoz.patrick.lab6.Ex4;

import java.util.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConsoleMenu {
    public static void main(String args[]) throws IOException {

        Dictionary dic=new Dictionary();
        char raspuns;
        String linie, explic;
        Definition exp;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga cuvant");
            System.out.println("c - Cauta cuvant");
            System.out.println("l - Listeaza dictionar");
            System.out.println("e - Iesi");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch(raspuns) {
                case 'a': case 'A':
                    System.out.println("Introduceti cuvantul:");
                    linie = fluxIn.readLine();
                    if( linie.length()>1) {
                        System.out.println("Introduceti definitia:");
                        explic = fluxIn.readLine();
                        dic.addWord(new Word(linie),new Definition(explic));
                    }
                    break;
                case 'c': case 'C':
                    System.out.println("Cuvant cautat:");
                    linie = fluxIn.readLine();
                    if( linie.length()>1) {
                        Word x = new Word(linie);
                        exp = dic.getDefinition(x);
                        if (exp == null)
                            System.out.println("Cuvantul nu a fost gasit!");
                        else
                            System.out.println("Definitie:"+exp.description);
                    }
                    break;
                case 'l': case 'L':
                    System.out.println("Afiseaza:");
                    dic.printDictionary();
                    break;

            }
        } while(raspuns!='e' && raspuns!='E');
        System.out.println("Program terminat.");
    }
}
