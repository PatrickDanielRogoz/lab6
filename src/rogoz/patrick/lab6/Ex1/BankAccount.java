package rogoz.patrick.lab6.Ex1;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    String owner;
    public double balance;
    public BankAccount(String owner,double balance){
        this.balance=balance;
        this.owner=owner;
    }
    public int compareTo(BankAccount b){
        return this.owner.compareTo(b.owner);
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    void withdraw(double amount){
        this.balance-=amount;
    }
    void deposit(double amount){
        this.balance+=amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return  ((BankAccount) o).balance==balance;

    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }


}
